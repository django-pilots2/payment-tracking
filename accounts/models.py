from django.db import models
from general.models import BaseModel, Country

# Create your models here.
GROUP_ROLES = (
    ('admin', 'Admin'),
    ('participant', 'Participant'),
)

class UserProfile(BaseModel):
    user = models.OneToOneField("auth.User",on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, db_index=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    class Meta:
        db_table = 'accounts_user'
        verbose_name = 'user profile'
        verbose_name_plural = 'users profile'
        ordering = ('-date_added',)


class Group(BaseModel):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'accounts_group'
        verbose_name = 'group'
        verbose_name_plural = 'groups'
        ordering = ('-date_added',)


class UsersGroup(BaseModel):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    role = models.CharField(choices=GROUP_ROLES, default='participant')

    class Meta:
        db_table = 'accounts_users_group'
        verbose_name = 'user group'
        verbose_name_plural = 'users group'