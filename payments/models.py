from django.db import models
from general.models import BaseModel
from accounts.models import UserProfile, UsersGroup


class Category(BaseModel):
    name = models.CharField(max_length=255)

    class Meta:
        db_name = 'payments_category'
        verbose_name = 'category'
        verbose_name_plural = 'categories'
        ordering = ('-date_added')


class Transaction(BaseModel):
    transaction_id = models.CharField(max_length=255)
    user_id = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    group_id = models.ForeignKey(UsersGroup, on_delete=models.CASCADE, null=True, blank=True)
    amount = models.CharField(max_length=255)

    class Meta:
        db_name = 'payments_transaction'
        verbose_name = 'transaction'
        verbose_name_plural = 'transactions'
        ordering = ('-date_added')